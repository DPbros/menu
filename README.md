**Тестовое задание для хакатона BEST'Hack**

Для запуска dev версии необходимы наличие npm, react-native-cli, для IOS среда разработки XCode, для Android Android Studio или физическое устройство с ОС Android

Полный цикл установки:
```
Для начала необходимо наличие npm
Для установки React Native запустите в терминале команду npm install -g react-native-cli
Также установите Android Studio если используете эмулятор для Android и проверьте наличие последней версии XCode
Если отсутствует, установите переменную окружения ANDROID_HOME

Для полной подготовки проекта откройте терминал и выполните:
git clone https://gitlab.com/DPbros/menu.git
cd menu
npm install
react-native link
```


*Для сборки Android версии выполить следующее*
```
react-native run-android
```


Для IOS возможно потребуется дополнительная конфигурация через XCode

*Для запуска IOS версии выполнить следующее*
```
Либо react-native run-ios, либо запустить Menu.xcodeproj 
```
