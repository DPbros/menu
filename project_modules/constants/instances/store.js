import {applyMiddleware, createStore} from 'redux';
import thunkMiddleware from 'redux-thunk'

import {rootReducer} from '../../functions/reducers/root_reducer';
import {DEFAULT_STATE} from "../init_constants";

import {persistReducer, persistStore} from 'redux-persist'
import storage from 'redux-persist/lib/storage'

const persistConfig = {
    key: 'root',
    storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(persistedReducer, {}, applyMiddleware(thunkMiddleware));
export const persistor = persistStore(store);