/**
 * Default store instance
 */
export const DEFAULT_STATE = {};

/**
 * Actions
 * @type {boolean} notification
 */
export const DATA = 'DATA';

/**
 * Firebase keys
 * @type {string}
 */
export const API_KEY = "AIzaSyDpde5W-_TwuRXdI1o6K1S9iGwKlYLZXgY";
export const AUTH_DOMAIN = "menu-131f5.firebaseapp.com";
export const DATABASE_URL = "https://menu-131f5.firebaseio.com";
export const PROJECT_ID = "menu-131f5";
export const STORAGE_BUCKET = "menu-131f5.appspot.com";
export const MESSAGING_SENDER_ID = "146765111893";

export const VERSION = "0.0.1 (1)";