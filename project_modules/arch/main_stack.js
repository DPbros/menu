import {createStackNavigator} from 'react-navigation';
import RoomsScreen from './rooms';
import MenuScreen from './menu_screen'
import Final from './final'

const MainStack = createStackNavigator({
    RoomsScreen: {screen: RoomsScreen},
    MenuScreen: {screen: MenuScreen},
    Final: {screen: Final}
}, {
    initialRouteName: 'RoomsScreen',
    mode: 'modal',
    headerMode: 'none'
});

export default MainStack;