import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Button} from "native-base";
import {connect} from "react-redux";

import CustomHeader from "./components/custom_header";
import BorderView from "./components/bordersView";
import {coefficient} from "../constants/values";

const mapStateToProps = state => ({
    data: state.DataReducer.data
});

const mapDispatchToProps = dispatch => ({});

class Final extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let protein, fats, carbons, kcal, weight, price;
        protein = fats = carbons = kcal = weight = price = 0;
        let data = this.props.navigation.getParam('data', []);
        data.forEach(e => {
            protein += e.protein;
            fats += e.fats;
            carbons += e.carbons;
            kcal += e.kcal;
            weight += e.weight;
            price += e.price;
        });

        return (
            <View>
                <CustomHeader title="Чек" leftTitle={'Назад'} left={() => this.props.navigation.goBack()}/>
                <View style={styles.container}>
                    <BorderView left={'БЖУ'} right={protein + '/' + fats + '/' + carbons}/>
                    <BorderView left={'Ккал'} right={kcal}/>
                    <BorderView left={'Вес'} right={weight}/>
                    <BorderView left={'Цена'} right={price}/>
                    <View style={{width: '100%', justifyContent: 'center'}}>
                        <Button block style={{backgroundColor: '#56ff54', marginHorizontal: 10}} onPress={() => this.props.navigation.goBack()}>
                            <Text>Готово</Text>
                        </Button>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: coefficient * 150,
        paddingTop: 5,
        justifyContent: 'flex-start'
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(Final)