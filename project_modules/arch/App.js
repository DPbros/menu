import React from 'react';

import {createAppContainer, createStackNavigator} from 'react-navigation';
import {persistor, store} from "../constants/instances/store";
import {Provider} from "react-redux";
import {PersistGate} from 'redux-persist/integration/react';
import {Root as RootBase} from "native-base";
import MainStack from './main_stack';

const Root = createStackNavigator({
  MainStack: {screen: MainStack},
}, {
  initialRouteName: 'MainStack',
  mode: 'modal',
  headerMode: 'none'
});

const AppContainer = createAppContainer(Root);

console.disableYellowBox = false;

class App extends React.Component {
  render() {
    return (
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <RootBase>
              <AppContainer/>
            </RootBase>
          </PersistGate>
        </Provider>
    )
  }
}

export default App