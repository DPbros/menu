import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Button, Accordion} from 'native-base';
import {connect} from "react-redux";

import {getData} from '../functions/actions'
import ShopView from "./components/shop_view";
import CustomHeader from "./components/custom_header";
import Food from "./components/food";

const mapStateToProps = state => ({
    data: state.DataReducer.data
});

const mapDispatchToProps = dispatch => ({});

class RoomsScreen extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            save: []
        };

        this.data = [];
        let y = props.data[1] ? props.data[1] : props.data.rooms;
        let z = props.data[0] ? props.data[0] : props.data.plates;
        let a = typeof y[0].days === 'object' ? Object.values(y[0].days) : y[0].days;
        a.forEach((e, index) => {
            let x = typeof e === 'object' ? Object.values(e) : e;
            let content = [];
            x.forEach(i => {
                content.push(z[i])
            });
            this.data.push({title: this.days[index + 1], content: content})
        })
    }

    days = {
        1: 'Понедельник',
        2: 'Вторник',
        3: 'Среда',
        4: 'Четверг',
        5: 'Пятница',
        6: 'Суббота',
        7: 'Воскресенье'
    };

    render() {
        let data = this.props.data ? typeof this.props.data === 'object' ? Object.values(this.props.data) : this.props.data : [];
        return (
            <View>
                <CustomHeader title="Меню" leftTitle={'Назад'} left={() => this.props.navigation.goBack()}
                              rightTitle={'Выбрано ' + this.state.save.length}
                              right={() => this.props.navigation.navigate('Final', {
                                  data: this.state.save
                              })}/>
                <View style={styles.container}>
                    <Accordion dataArray={this.data}
                               expanded={(new Date()).getDay() - 1}
                               renderContent={(item) => {
                                   return (
                                       <View style={{flex: 1}}>
                                           {item.content.map((e, index, a) => {
                                               return (
                                                   <Food data={e} onPress={() => {
                                                       if (Object.values(this.days).findIndex(e => e === item.title) === (new Date()).getDay() - 1)
                                                           if (!this.state.save.includes(e)) {
                                                               let r = this.data.findIndex(z => z.title === item.title);
                                                               this.data[r].content[index].color = true;
                                                               let q = this.state.save;
                                                               q.push(e);
                                                               this.setState({save: q})
                                                           } else {
                                                               let r = this.data.findIndex(z => z.title === item.title);
                                                               this.data[r].content[index].color = false;
                                                               let q = this.state.save;
                                                               let i = q.findIndex(f => f.id === e.id);
                                                               q.splice(i, 1);
                                                               this.setState({save: q})
                                                           }
                                                   }}/>
                                               )
                                           })}
                                       </View>
                                   )
                               }}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {},
});

export default connect(mapStateToProps, mapDispatchToProps)(RoomsScreen)