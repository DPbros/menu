import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Button, Tab} from 'native-base';
import {connect} from "react-redux";

import {getData} from '../functions/actions'
import ShopView from "./components/shop_view";
import CustomHeader from "./components/custom_header";

const mapStateToProps = state => ({
    data: state.DataReducer.data
});

const mapDispatchToProps = dispatch => ({
    getData: () => dispatch(getData())
});

class RoomsScreen extends React.Component {
    constructor() {
        super();
    }

    componentWillMount(): void {
        this.props.getData()
    }

    render() {
        let data = this.props.data ? typeof this.props.data === 'object' ? Object.values(this.props.data) : this.props.data : [];
        return (
            <View>
                <CustomHeader title="Столовые"/>
                <View style={styles.container}>
                    {data[1].map(e => {
                        return (<ShopView title={e.title} onPress={() => this.props.navigation.navigate('MenuScreen')} open={e.open} close={e.close}/>)
                    })}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(RoomsScreen)