import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {coefficient} from "../../constants/values";

class BorderView extends React.Component {
    render() {
        return (
            <View style={[{flexDirection: 'row', marginHorizontal: 5}, this.props.style]}>
                <Text style={styles.leftInfo}>{this.props.left}</Text>
                <Text style={styles.rightInfo}>{this.props.right}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    leftInfo: {
        flex: 1,
        fontSize: coefficient * 14
    },
    rightInfo: {
        flex: 1,
        textAlign: 'right',
        fontSize: coefficient * 14
    }
});

BorderView.defaultProps = {
    style: {
        flex: 1
    },
    left: '',
    right: ''
};

export default BorderView