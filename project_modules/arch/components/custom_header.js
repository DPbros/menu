import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Body, Button, H2, Header, Left, Right} from "native-base";

class CustomHeader extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Header hasText noShadow transparent style={{backgroundColor: '#28bf26'}}>
                {!!this.props.leftTitle && <Left>
                    <Button hasText style={{backgroundColor: '#fff', marginLeft: -3, borderRadius: 5}}
                            onPress={() => this.props.left()}>
                        <Text>{this.props.leftTitle}</Text>
                    </Button>
                </Left>}
                <Body>
                <H2 style={{textAlign: 'right', color: '#fff'}}>{this.props.title}</H2>
                </Body>
                {!!this.props.rightTitle && <Right>
                    <Button hasText style={{backgroundColor: '#fff', borderRadius: 5}}
                            onPress={() => this.props.right()}>
                        <Text>{this.props.rightTitle}</Text>
                    </Button>
                </Right>}
            </Header>
        );
    }
}

CustomHeader.defaultProps = {
    rightTitle: "",
    leftTitle: "",
    right: () => 1,
    left: () => 1
};

const styles = StyleSheet.create({});

export default CustomHeader;