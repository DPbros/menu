import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {Accordion, Icon} from "native-base";
import {coefficient} from "../../constants/values";
import BorderView from "./bordersView";

class Food extends React.Component {
    render() {
        let color = {};
        if (!this.props.data.color) color = {backgroundColor: '#fff'}; else color = {backgroundColor: '#f7f1ca'};
        return (
            <TouchableOpacity style={[styles.container, color]} onPress={() => this.props.onPress()}>
                <Text style={styles.topInfo}>{this.props.data.title}</Text>
                <View style={{flexDirection: 'row', borderBottomWidth: 1, borderColor: '#9f9f9f'}}>
                    <BorderView left={'БЖУ'} right={this.props.data.protein + '/' + this.props.data.fats + '/' + this.props.data.carbons}/>
                    <BorderView left={'Ккал'} right={this.props.data.kcal} style={{flex: 1, borderLeftWidth: 1, paddingLeft: 3, borderColor: '#9f9f9f'}}/>
                </View>
                <View style={{flexDirection: 'row'}}>
                    <BorderView left={'Вес'} right={this.props.data.weight}/>
                    <BorderView left={'Цена'} right={this.props.data.price} style={{flex: 1, borderLeftWidth: 1, paddingLeft: 3, borderColor: '#9f9f9f'}}/>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        height: 65 * coefficient,
        borderRadius: 10,
        overflow: 'hidden',
        borderColor: '#b0b0b0',
        borderWidth: 0.7,
        padding: 5,
        marginVertical: 3,
        marginHorizontal: 4
    },
    topInfo: {
        flex: 1,
        fontSize: 13 * coefficient,
        fontWeight: '500'
    },
    bottomInfo: {
        flex: 1,
    },
});

Food.defaultProps = {
    color: false
};

export default Food