import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {Accordion, Icon} from "native-base";
import {coefficient} from "../../constants/values";

class ShopView extends React.Component {
    render() {
        let text, color;
        let currentTime = (new Date()).getHours() * 60 + (new Date()).getMinutes();
        let open = this.props.open.split(':');
        let close = this.props.close.split(':');

        if (currentTime >= (parseInt(open[0]) * 60 + parseInt(open[1])) && currentTime <= (parseInt(close[0]) * 60 + parseInt(close[1]))) {
            color = {color: '#03ff03'};
            text = 'Открыто'
        } else {
            color = {color: '#f00303'};
            text = 'Закрыто'
        }
        return (
            <TouchableOpacity style={styles.container} onPress={() => this.props.onPress()}>
                <Text style={styles.topInfo}>{this.props.title}</Text>
                <Text style={[styles.bottomInfo, color]}>{text}</Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        height: 45 * coefficient,
        borderRadius: 10,
        overflow: 'hidden',
        borderColor: '#b0b0b0',
        borderWidth: 0.7,
        padding: 5,
        marginVertical: 3
    },
    topInfo: {
        flex: 1,
        fontSize: 13 * coefficient,
        fontWeight: '500'
    },
    bottomInfo: {
        flex: 1,
    },
});

export default ShopView