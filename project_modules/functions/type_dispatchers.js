import * as types from "../constants/init_constants";

export const data = (data) => ({
    type: types.DATA,
    data: data
});