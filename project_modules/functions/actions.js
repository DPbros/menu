import * as firebase from './firebase';
import {auth} from '../constants/instances/firebase';
import * as types from './type_dispatchers'

export function getData(path) {
    return (dispatch) => {
        firebase.fetchData((success, data) => {
            if (success)
                dispatch(types.data(data));
        });
    };
}