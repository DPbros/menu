import {auth, database, secondPort, storage} from "../constants/instances/firebase";

export function fetchData(callback) {
    database.ref().on('value', (snapshot) => {
        callback(snapshot.exists(), snapshot.val())
    })
}