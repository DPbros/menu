import {combineReducers} from "redux";
import {DATA} from "../../constants/init_constants";

export const rootReducer = combineReducers({
    DataReducer
});

function DataReducer (state = {data: null}, action) {
    switch (action.type) {
        case DATA: {
            return {...state, data: action.data};
        }
        default: {
            return state;
        }
    }
}