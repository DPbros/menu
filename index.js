import {AppRegistry} from 'react-native';
import App from './project_modules/arch/App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
